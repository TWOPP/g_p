<?php

namespace AppBundle\Controller;

use AppBundle\Entity\BasePoint;
use AppBundle\Entity\Population;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\UploadCSVType;

class DefaultController extends Controller
{
    const DIR = '/../web/upload/';

    /** @var  ArrayCollection */
    private $operations;

    public function __construct(){
        $this->points = new ArrayCollection();
        $this->originPoints = new ArrayCollection();
        $this->operations = new ArrayCollection();
    }

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $file = $request->request->get('fileName');
        $generate = $request->request->get('generate');
        $parameters = $request->request->get('parameters');
        $operations = $request->request->get('operations');
        $strResult = null;

        $form = $this->createForm(UploadCSVType::class, null, ['method' => 'POST']);

        $upload = $this->uploadFile($request, $form);
        if($upload != null)
            $file = $upload;

        $files = $this->scanFiles();
        $this->readFile($file, $files);

        $populationSize = $this->getParameters($parameters, 'size');

        $maxDepth = $this->getParameters($parameters, 'depth');

        $generationNumber = $this->getParameters($parameters, 'generations');

        $operations = $this->getOperations($operations);
        if($generate == 'generate') {
            $strResult = $this->generate($populationSize, $maxDepth, $generationNumber);
        }


        return $this->render('AppBundle:Default:index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
            'form' => $form->createView(),
            'files' => $files,
            'fileSelect' => $file,



            'populationSize' => $populationSize,
            'maxDepth' => $maxDepth,
            'generationNumber' => $generationNumber,
            'operations' => $operations,
            'logs' => (isset($strResult['logs'])) ? $strResult['logs'] : null,
            'result' => (isset($strResult['result'])) ? $strResult['result'] : null,
            'describe' => (isset($strResult['describe'])) ? $strResult['describe'] : null
        ]);
    }




    private function generate($populationSize, $maxDepth, $generationNumber){
        set_time_limit(0);
        $sizeOfPopulation = $populationSize;
        $depth = $maxDepth;
        $numberOfGeneration = $generationNumber;
        $useAllVariable = true; // sztywno ustawione używanie wszystkich zmiennych
        $numberOfVariables = sizeof($this->points->get(0)->getX());

        $logs = [];

        $varID = 1; //zmienna ustawiająca znaczaca zmienna

        $p1 = new Population();

        $p1->generate($sizeOfPopulation, $depth, $this->operations, $numberOfVariables);

        for($i = 0; $i < $numberOfGeneration; $i ++){
            $p1->countFitness($this->points, $numberOfVariables, $varID, $useAllVariable);

            $f = $p1->getBestSolution()->getF();
            $logs[] .=  $f;

            $p1->run($depth, $this->operations, $numberOfVariables);
        }

        $p1->countFitness($this->points, $numberOfVariables, $varID, $useAllVariable);

        $f2 = $p1->getBestSolution()->getF();
        $string2 = $numberOfGeneration . ' : ' . $f2;

        $describe = $p1->getBestSolution()->getHead()->getDescription();
        $result = $p1->getBestSolution()->getF();

        return[
            'logs' => $logs,
            'str2' => $string2,
            'result' => $result,
            'describe' => $describe
        ];

    }









    /**
     * @param Request $request
     * @param Form $form
     * @return null
     */
    private function uploadFile(Request $request, Form $form){
        $file = null;


        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $fileUpload = $form->getData()['file'];

            $fileName= $fileUpload->getClientOriginalName();

            $fileUpload->move(
                $this->get('kernel')->getRootDir().self::DIR,
                $fileName
            );

            $file = $fileName;
        }

        return $file;
    }


    /**
     * @return array
     */
    private  function scanFiles(){
        $path = $this->get('kernel')->getRootDir().self::DIR;

        $returnArray = [];

        foreach(scandir($path) as $file){
            if(!in_array($file, ['.', '..']) && !is_dir($path.DIRECTORY_SEPARATOR.$file) && $file != '.DS_Store')
                $returnArray[] = $file;
        }

        return $returnArray;
    }

    /** @var  ArrayCollection */
    private $points;

    /** @var  ArrayCollection */
    private $originPoints;

    private function readFile($filename, $files){

        if($filename == null && sizeof($files) == 0)
            return null;

        if($filename == null)
            $filename = $files[0];

        ini_set('auto_detect_line_endings', true);

        $this->points->clear();

        $file = fopen($this->get('kernel')->getRootDir().self::DIR.$filename, 'r');

        $line = fgetcsv($file, 10000, ',');
        if($line != null){

            /**
             * ODCZYT NAGŁÓWKÓW
             */



            while($line = fgetcsv($file, 10000, ',')){

                $x = [];
                $y = 0;

                for($i = 0; $i < sizeof($line); $i++){

                    if(is_numeric(trim($line[$i]))){

                        if($i < sizeof($line) - 1){

                            $x[$i] = floatval(trim($line[$i]));
                        }else{

                            $y = trim($line[$i]);
                        }
                    }
                    else{

                        $str2 = trim(str_replace('.', ',', $line[$i]));

                        if(is_numeric($str2)){

                            if($i < sizeof($line) - 1){

                                $x[$i] = floatval($str2);
                            }
                        }else{
                            //hu wiw
                        }
                    }

                }

                $basePoint = new BasePoint();
                $basePoint->setY($y);
                $basePoint->setX($x);
                $this->points->add($basePoint);
            }

        }

    }





    private function getOperations($inputs){
        $op = ['ADD', 'DIV', 'DIVp', 'MUL', 'SUB', 'POW', 'POWp', 'SIN', 'COS', 'EXP', 'LOG', 'LOGp'];

        if(sizeof($inputs) == 0){
            return ['ADD', 'DIVp', 'MUL', 'SUB', 'POWp', 'SIN', 'COS', 'EXP', 'LOGp'];
        }


        $returnArray =[];
        foreach($op as $value){
            if(key_exists($value, $inputs)){
                $this->operations->add($value);
                $returnArray[] = $value;
            }
        }

        return $returnArray;

    }

    private function getParameters($parameters, $parameter){

        if(isset($parameters[$parameter])){
            $result = $parameters[$parameter];

            if(!is_numeric($result)){
                $result = $this->defaultParametersValue($parameter);
            }else{
                $result = intval($result);
            }
        }else{
            $result = $this->defaultParametersValue($parameter);
        }

        return $result;

    }

    private function defaultParametersValue($parameter){
        switch($parameter){
            case 'size':
                return 20;
                break;
            case 'depth':
                return 6;
                break;
            case 'generations':
                return 30;
                break;
            default:
                return 20;
                break;
        }
    }
}
