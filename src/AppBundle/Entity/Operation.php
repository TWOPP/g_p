<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Operation
 */
class Operation
{

    public function result($values = null, $data = null, &$error = null){
        $error = false;
        return 1;
    }

    public function isTerminated(){
        return false;
    }

    public function __toString()
    {
        return 'OP';
    }

    public function numberOfChildren(){
        return 0;
    }

    public function getCloneOperation(){
        return null;
    }

    protected static function random(){
        return mt_rand();
    }

}
