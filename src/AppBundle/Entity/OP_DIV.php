<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * OP_DIV
 */
class OP_DIV extends Operation
{

    /**
     * @param null $values
     * @param null $data
     * @param null $error
     * @return float|int
     */
    public function result($values = null, $data = null, &$error = null){

        if($data[1] == 0){

            $error = true;
            return 0;
        }else{

            $error = false;
            return $data[0] /$data[1];
        }
    }

    /**
     * @return bool
     */
    public function isTerminated(){
        return false;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return 'DIV';
    }

    /**
     * @return int
     */
    public function numberOfChildren(){
        return 2;
    }

    /**
     * @return OP_DIV
     */
    public function getCloneOperation(){
        return new OP_DIV();
    }

}
