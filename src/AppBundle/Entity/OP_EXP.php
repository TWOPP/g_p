<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * OP_EXP
 */
class OP_EXP extends Operation
{

    /**
     * @param null $values
     * @param null $data
     * @param null $error
     * @return float|int
     */
    public function result($values = null, $data = null, &$error = null){

        $d = exp($data[0]);

        $error = false;
        return $d;
    }

    /**
     * @return bool
     */
    public function isTerminated(){
        return false;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return 'EXP';
    }

    /**
     * @return int
     */
    public function numberOfChildren(){
        return 1;
    }

    /**
     * @return OP_EXP
     */
    public function getCloneOperation(){
        return new OP_EXP();
    }

}
