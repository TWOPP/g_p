<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Node
 */
class Node
{

    /**
     * @return array
     */
    public function getChildren(): array
    {
        return $this->children;
    }

    /**
     * @param array $children
     */
    public function setChildren(array $children)
    {
        $this->children = $children;
    }

    /**
     * @return Node
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param Node $parent
     */
    public function setParent(Node $parent)
    {
        $this->parent = $parent;
    }

    /**
     * @return Operation
     */
    public function getNodeOperation(): Operation
    {
        return $this->operation;
    }

    /**
     * @param Operation $operation
     */
    public function setNodeOperation(Operation $operation)
    {
        $this->operation = $operation;
    }

    /**
     * @return int
     */
    public function getDepth(): int
    {
        return $this->depthParam;
    }

    /**
     * @param int $depthParam
     */
    public function setDepth(int $depthParam)
    {
        $this->depthParam = $depthParam;
    }

    /** @var array  */
    private $children;
    /** @var Node  */
    private $parent;
    /** @var Operation  */
    private $operation;
    /** @var int  */
    private $depthParam = 0;

    /**
     * Node constructor.
     * @param Node|null $parent
     */
    public function __construct(Node $parent = null){
        $this->parent = $parent;
        $this->children[0] = null;
        $this->children[1] = null;
    }

    /**
     * @param Node|null $parent
     * @return Node
     */
    public function cloneNode(Node $parent = null){
        $newNode = new Node($parent);
        $newNode->setNodeOperation($this->operation->getCloneOperation());//TODO metoda klonowania

        $newChildren[0] = new Node();
        $newChildren[1] = new Node();

        if($this->children[0]==null){
            $newChildren[0] = null;
        }else{
            $newChildren[0] = $this->children[0]->cloneNode($newNode);
        }

        if($this->children[1] == null){
            $newChildren[1] = null;
        }else{
            $newChildren[1] = $this->children[1]->cloneNode($newNode);
        }

//        $newNode->setDepth($this->depthParam);
        $newNode->setChildren($newChildren);

        return $newNode;
    }

    /**
     * @param int $value
     * @return int
     */
    public function countNodes(int $value){
        $resultValue = $value+1;

        if($this->children[0] != null){
            $resultValue = $this->children[0]->countNodes($resultValue);
        }

        if($this->children[1] != null){
            $resultValue = $this->children[1]->countNodes($resultValue);
        }

        return $resultValue;
    }

    /**
     * @param int $length
     * @return int
     */
    public function getPathLength(int $length){
        $result = $length + 1;

        $this->depthParam = $result;

        $r1 = 0;
        $r2 = 0;

        if($this->children[0] != null){
            $r1 = $this->children[0]->getPathLength($result);
        }

        if($this->children[1] != null){
            $r2 = $this->children[1]->getPathLength($result);
        }

        if($r1 > $result){
            $result = $r1;
        }

        if($r2 > $result){
            $result = $r2;
        }

        return $result;
    }

    /**
     * @param int|null $value
     * @param int|null $selected
     * @return $this|Node|null
     */
    public function selectNode(int $value = null, int $selected = null){

        $resultValue = $value + 1;

        if($value == $selected){
            return $this;
        }else{
            /** @var Node $n */
            $n = null;

            if($this->children[0] != null){
                $n = $this->children[0]->selectNode($resultValue, $selected);

                if($n != null){

                    return $n;
                }else{

                    $resultValue = $this->children[0]->countNodes($resultValue);
                }

            }

            if($this->children[1] != null){

                $n = $this->children[1]->selectNode($resultValue, $selected);
                if($n != null){

                    return $n;
                }
            }

            return $n;
        }
    }

    /**
     * @param null $values
     * @param null $error
     * @return int
     */
    public function count($values = null, &$error = null){

        if($this->operation->isTerminated()){

            return $this->operation->result($values, null, $error);
        }else{

            if($this->operation->numberOfChildren() == 2){

                if($this->children[0] == null || $this->children[1] == null){

                    $error = true;
                    return 0;
                }else {

                    $error1 = null;
                    $error2 = null;

                    $data[0] = $this->children[0]->count($values, $error1);
                    $data[1] = $this->children[1]->count($values, $error2);

                    if ($error1 == true || $error2 == true) {

                        $error = true;
                        return 0;
                    } else {
                        return $this->operation->result($values, $data, $error);
                    }
                }
            }else{

                if($this->operation->numberOfChildren() == 1){

                    if($this->children[0] == null){

                        $error = true;
                        return 0;
                    }else{

                        $error1 = null;

                        $data[0] = $this->children[0]->count($values, $error1);

                        if($error1 == true){

                            $error = true;
                            return 0;
                        }else{
                            return $this->operation->result($values,$data,$error);
                        }
                    }
                }else{

                    $error = true;
                    return 0;
                }
            }
        }
    }

    /**
     * @return Operation|string
     */
    public function getDescription(){

        if($this->operation->isTerminated()){

            return $this->operation;
        }else{

            if($this->operation->numberOfChildren() == 1){

                if($this->children[0] != null){

                    return $this->operation->__toString() . " ( " . $this->children[0]->getDescription() . " ) ";
                }else{

                    return $this->operation->__toString() . + 'E';
                }
            }else{

                if($this->operation->numberOfChildren() == 2){

                    if($this->children[0] != null && $this->children[1] != null){
                        return $this->operation->__toString() . ' ( ' . $this->children[0]->getDescription() . ' , ' . $this->children[1]->getDescription() . ' ) ';
                    }else{

                        return $this->operation->__toString() . 'E';
                    }
                }
            }
        }

        return $this->operation->__toString() . 'E';
    }

    public function init(int $numberOfVariables = null){
        $x = [];
    }



}
