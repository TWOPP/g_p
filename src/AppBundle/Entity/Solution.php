<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Solution
 */
class Solution
{
    /**
     * @return Node
     */
    public function getHead(): Node
    {
        return $this->head;
    }

    /**
     * @param Node $head
     */
    public function setHead(Node $head)
    {
        $this->head = $head;
    }

    /**
     * @return float
     */
    public function getF(): float
    {
        return $this->f;
    }

    /**
     * @param float $f
     */
    public function setF(float $f)
    {
        $this->f = $f;
    }


    /** @var Node  */
    private $head;
    /** @var float  */
    private $f;


    /**
     * Solution constructor.
     */
    public function __construct(){
        $this->head = null;
        $this->f = -1;
    }

    /**
     * @return Solution
     */
    public function getCloned(){
        $solution = new Solution();
        $solution->setF($this->f);
        $solution->setHead($this->head->cloneNode(null));

        return $solution;
    }


}
