<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * OP_LOG
 */
class OP_LOG extends Operation
{

    /**
     * @param null $values
     * @param null $data
     * @param null $error
     * @return float|int
     */
    public function result($values = null, $data = null, &$error = null){

        $d = log10($data[0]);

        if(!is_numeric($d)){

            $error = true;
            return 0;
        }else{

            $error = false;
            return $d;
        }
    }

    /**
     * @return bool
     */
    public function isTerminated(){
        return false;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return 'LOG';
    }

    /**
     * @return int
     */
    public function numberOfChildren(){
        return 1;
    }

    /**
     * @return OP_LOG
     */
    public function getCloneOperation(){
        return new OP_LOG();
    }

}
