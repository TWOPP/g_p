<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * OP_T_D
 */
class OP_T_D extends Operation
{
    /**
     * @return float
     */
    public function getValue(): float
    {
        return $this->value;
    }

    /**
     * @param float $value
     */
    public function setValue(float $value)
    {
        $this->value = $value;
    }

    /** @var float  */
    private $value = 0;

    /**
     * @param float|null $scope
     */
    public function init(float $scope = null){
        $this->value = mt_rand() * 2 * $scope - $scope;
    }

    /**
     * @param null $values
     * @param null $data
     * @param null $error
     * @return float
     */
    public function result($values = null, $data = null, &$error = null){
        $error = false;
        return $this->value;
    }

    /**
     * @return bool
     */
    public function isTerminated(){
        return true;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)$this->value;
    }

    /**
     * @return int
     */
    public function numberOfChildren(){
        return 0;
    }

    /**
     * @return OP_T_D
     */
    public function getCloneOperation(){
        $newOP = new OP_T_D();
        $newOP->setValue($this->value);
        return $newOP;
    }

}
