<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * OP_POW_P
 */
class OP_POW_P extends Operation
{

    /**
     * @param null $values
     * @param null $data
     * @param null $error
     * @return mixed
     */
    public function result($values = null, $data = null, &$error = null){

        $d = pow($data[0], $data[1]);

        if(is_nan($d)){
            return 1;
        }else{
            return $d;
        }
    }

    /**
     * @return bool
     */
    public function isTerminated(){
        return false;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return 'POWp';
    }

    /**
     * @return int
     */
    public function numberOfChildren(){
        return 2;
    }

    /**
     * @return OP_POW_P
     */
    public function getCloneOperation(){
        return new OP_POW_P();
    }

}
