<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * OP_DIV_P
 */
class OP_DIV_P extends Operation
{

    /**
     * @param null $values
     * @param null $data
     * @param null $error
     * @return float|int
     */
    public function result($values = null, $data = null, &$error = null){

        $error = false;

        if($data[1] == 0){

            return 1;
        }else{
            return $data[0] / $data[1];
        }
    }

    /**
     * @return bool
     */
    public function isTerminated(){
        return false;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return 'DIVp';
    }

    /**
     * @return int
     */
    public function numberOfChildren(){
        return 2;
    }

    /**
     * @return OP_DIV_P
     */
    public function getCloneOperation(){
        return new OP_DIV_P();
    }

}
