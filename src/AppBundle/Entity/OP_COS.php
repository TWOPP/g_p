<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * OP_COS
 */
class OP_COS extends Operation
{

    /**
     * @param null $values
     * @param null $data
     * @param null $error
     * @return float
     */
    public function result($values = null, $data = null, &$error = null){

        $d = cos($data[0]);

        $error = false;
        return $d;
    }

    /**
     * @return bool
     */
    public function isTerminated(){
        return false;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return 'COS';
    }

    /**
     * @return int
     */
    public function numberOfChildren(){
        return 1;
    }

    /**
     * @return OP_COS
     */
    public function getCloneOperation(){
        return new OP_COS();
    }

}
