<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * OP_T_V
 */
class OP_T_V extends Operation
{
    /**
     * @return int
     */
    public function getValueID(): int
    {
        return $this->valueID;
    }

    /**
     * @param int $valueID
     */
    public function setValueID(int $valueID)
    {
        $this->valueID = $valueID;
    }

    /** @var int  */
    private $valueID = 0;

    /**
     * @param int $numbersOfValues
     */
    public function init(int $numbersOfValues){

        $this->valueID = mt_rand(0, $numbersOfValues-1);

        if($this->valueID != 0){
            $a = 0;
            $a++;
        }
    }

    /**
     * @param null $values
     * @param null $data
     * @param null $error
     * @return mixed
     */
    public function result($values = null, $data = null, &$error = null){
        $error = false;
        return $values[$this->valueID];
    }

    /**
     * @return bool
     */
    public function isTerminated(){
        return true;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return 'X' . (string)($this->valueID+1);
    }

    /**
     * @return int
     */
    public function numberOfChildren(){
        return 0;
    }

    /**
     * @return OP_T_V
     */
    public function getCloneOperation(){
        $newOP = new OP_T_V();
        $newOP->setValueID($this->valueID);
        return $newOP;
    }

}
