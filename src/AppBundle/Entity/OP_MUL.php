<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * OP_MUL
 */
class OP_MUL extends Operation
{

    /**
     * @param null $values
     * @param null $data
     * @param null $error
     * @return mixed
     */
    public function result($values = null, $data = null, &$error = null){

        $error = false;
        return $data[0] * $data[1];
    }

    /**
     * @return bool
     */
    public function isTerminated(){
        return false;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return 'MUL';
    }

    /**
     * @return int
     */
    public function numberOfChildren(){
        return 2;
    }

    /**
     * @return OP_MUL
     */
    public function getCloneOperation(){
        return new OP_MUL();
    }

}
