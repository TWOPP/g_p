<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BasePoint
 */
class BasePoint
{
    /** @var float  */
    private $y = 0;
    /** @var array  */
    private $x = [];

    public function init(int $numberOfVariables = null){
        $x = [];
    }

    /**
     * @return float
     */
    public function getY(): float
    {
        return $this->y;
    }

    /**
     * @param float $y
     */
    public function setY(float $y)
    {
        $this->y = $y;
    }

    /**
     * @return array
     */
    public function getValue(): array
    {
        return $this->value;
    }

    /**
     * @param array $value
     */
    public function setValue(array $value)
    {
        $this->value = $value;
    }

    /**
     * @return array
     */
    public function getX()
    {
            return $this->x;
    }

    /**
     * @param array $x
     */
    public function setX(array $x)
    {
        $this->x = $x;
    }



}
