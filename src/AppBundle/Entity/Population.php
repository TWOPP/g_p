<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Query\Expr\Base;
use AppBundle\Entity\OP_ADD;
use AppBundle\Entity\OP_COS;
use AppBundle\Entity\OP_DIV;
use AppBundle\Entity\OP_DIV_P;
use AppBundle\Entity\OP_EXP;
use AppBundle\Entity\OP_LOG;
use AppBundle\Entity\OP_LOG_P;
use AppBundle\Entity\OP_MUL;
use AppBundle\Entity\OP_POW;
use AppBundle\Entity\OP_POW_P;
use AppBundle\Entity\OP_SIN;
use AppBundle\Entity\OP_SUB;
use AppBundle\Entity\OP_T_D;
use AppBundle\Entity\OP_T_V;

/**
 * Population
 */
class Population
{

    public function __construct(){
        $this->populationList = new ArrayCollection();
    }

    /**
     * @return ArrayCollection
     */
    public function getPopulationList(): ArrayCollection
    {
        return $this->populationList;
    }

    /**
     * @param ArrayCollection $populationList
     */
    public function setPopulationList(ArrayCollection $populationList)
    {
        $this->populationList = $populationList;
    }

    /**
     * @return Solution
     */
    public function getBestSolution(): Solution
    {
        return $this->bestSolution;
    }

    /**
     * @param Solution $bestSolution
     */
    public function setBestSolution(Solution $bestSolution)
    {
        $this->bestSolution = $bestSolution;
    }

    /** @var  ArrayCollection */
    private $populationList;

    /** @var  Solution */
    private $bestSolution;

    public function generate(int $size, int $depth, ArrayCollection $operations, int $numberOfVariables){

        $this->populationList->clear();
        $grow = false;

        do{
            /** @var Solution $solution */
            $solution = null;

            if($grow){

                $grow = false;
                $solution = $this->createGrowMethod($depth, $operations, $numberOfVariables);
            }else{

                $grow = true;
                $solution = $this->createFullMethod($depth, $operations, $numberOfVariables);
            }

            $this->populationList->add($solution);

        }while($this->populationList->count() < $size);
    }

    /**
     * @param ArrayCollection $points
     * @param int $numberOfVariables
     * @param int $varID
     * @param bool $allVariables
     */
    public function countFitness(ArrayCollection $points, int $numberOfVariables, int $varID, bool $allVariables){

        for($i = 0; $i < $this->populationList->count(); $i++){

            $f = 0;

            for($j = 0; $j < $points->count(); $j++){

                $var = [];

                if($allVariables){

                    /** @var BasePoint $selectBasePoint */
                    $selectBasePoint = $points[0];

                    for($k = 0; $k < sizeof($selectBasePoint->getX()); $k++){

                        $var[$k] = $points->get($j)->getX()[$k];
                    }
                }else{

                    $var[0] = $points->get($j)->getX()[$varID];
                }


                $error = null;

                $result = $this->populationList->get($i)->getHead()->count($var, $error);

                if($error){

                    $f = -1;
                    break;
                }else{

                    $f = $f + ($points->get($j)->getY() - $result) * ($points->get($j)->getY() - $result);
                }
            }

            $f = $f / $points->count();

            $this->populationList->get($i)->setF($f);

            if($f >= 0){

                if($this->bestSolution == null){
                    $this->bestSolution = $this->populationList->get($i);
                }else{

                    if($f < $this->bestSolution->getF()){

                        $this->bestSolution = $this->populationList->get($i);
                    }
                }
            }
        }
    }

    /**
     * @param int $size
     * @return Solution
     */
    public function Tournament(int $size){
        set_time_limit(0);
        $currentSize = $size;

        if($currentSize > $this->populationList->count()){
            $currentSize = $this->populationList->count();

        }

        $selected = new ArrayCollection();

        for($i = 0; $i < $currentSize; $i++){

            $randomNumber = mt_rand(0, $this->populationList->count()-1);
            $solution = $this->populationList->get($randomNumber);

            $selected->add($solution);
            $this->populationList = $this->removeRowFromArray($this->populationList, $randomNumber);

        }

        /** @var Solution $best */
        $best = $selected->get(0);

        for($i = 0; $i < $selected->count(); $i++){

            if($selected->get($i)->getF() < $best->getF()){

                $best = $selected->get($i);
            }
        }

        for($i = 0; $i < $selected->count(); $i++){
            $this->populationList->add($selected->get($i));
        }

        return $best;
    }

    public function run(int $maxDepth, ArrayCollection $operations, int $numberOfVariables){

        $newPopulation = new ArrayCollection();

        do{

            $p1 = $this->Tournament($this->populationList->count() / 10);
            $p2 = $this->Tournament($this->populationList->count() / 10);

            $np1 = null;
            $np2 = null;

            $this->crossOver($p1, $p2, $maxDepth, $np1, $np2);

            $np3 = null;
            $np4 = null;

            $this->mutation($np1, $maxDepth, $operations, $numberOfVariables, $np3);
            $this->mutation($np2, $maxDepth, $operations, $numberOfVariables, $np4);

            $newPopulation->add($np3);
            $newPopulation->add($np4);

        }while($newPopulation->count() < $this->populationList->count());

        $this->populationList->clear();
        $this->populationList = $newPopulation;
    }

    /**
     * @param Node $node
     * @param int $depth
     * @param int $maxDepth
     * @param ArrayCollection $operations
     * @param int $numberOfVariables
     */
    public function createFullMethodNode(Node $node, int $depth, int $maxDepth, ArrayCollection $operations, int $numberOfVariables){

        $currentDepth = $depth + 1;

        $children[0] = new Node();
        $children[1] = new Node();

        if($currentDepth < $maxDepth){

            if($node->getNodeOperation()->numberOfChildren() == 1){

                $children[0] = new Node($node);
                $children[0]->setNodeOperation($this->randNonTerminated($operations));
                $children[0]->setDepth($currentDepth);

                $children[1] = null;
                $node->setChildren($children);
                $this->createFullMethodNode($children[0], $currentDepth, $maxDepth, $operations, $numberOfVariables);
            }else{

                $children[0] = new Node($node);
                $children[0]->setNodeOperation($this->randNonTerminated($operations));
                $children[0]->setDepth($currentDepth);

                $children[1] = new Node($node);
                $children[1]->setNodeOperation($this->randNonTerminated($operations));
                $children[1]->setDepth($currentDepth);

                $node->setChildren($children);
                $this->createFullMethodNode($children[0], $currentDepth, $maxDepth, $operations, $numberOfVariables);
                $this->createFullMethodNode($children[1], $currentDepth, $maxDepth, $operations, $numberOfVariables);
            }
        }else{

            if($node->getNodeOperation()->numberOfChildren() == 1){

                $children[0] = new Node($node);
                $children[0]->setNodeOperation($this->randTerminated($numberOfVariables));
                $children[0]->setDepth($currentDepth);

                $children[1] = null;
                $node->setChildren($children);
            }else{

                $children[0] = new Node($node);
                $children[0]->setNodeOperation($this->randTerminated($numberOfVariables));
                $children[0]->setDepth($currentDepth);

                $children[1] = new Node($node);
                $children[1]->setNodeOperation($this->randTerminated($numberOfVariables));
                $children[1]->setDepth($currentDepth);

                $node->setChildren($children);
            }
        }
    }

    /**
     * @param Node $node
     * @param int $depth
     * @param int $maxDepth
     * @param ArrayCollection $operations
     * @param int $numberOfVariables
     */
    public function createGrowMethodNode(Node $node, int $depth, int $maxDepth, ArrayCollection $operations, int $numberOfVariables){

        $currentDepth = $depth + 1;

        $children[0] = new Node();
        $children[1] = new Node();

        $r1 = mt_rand(0,2);
        $r2 = mt_rand(0,2);

        if($currentDepth >= $maxDepth){
            $r1 = 1;
            $r2 = 1;
        }

        if($r1 == 0){

            $children[0] = new Node($node);
            $children[0]->setNodeOperation($this->randNonTerminated($operations));
            $children[0]->setDepth($currentDepth);

            $children[1] = null;
            $node->setChildren($children);
            $this->createGrowMethodNode($children[0], $currentDepth, $maxDepth, $operations, $numberOfVariables);
        }else{

            $children[0] = new Node($node);
            $children[0]->setNodeOperation($this->randTerminated($numberOfVariables));
            $children[0]->setDepth($currentDepth);

            $children[1] = null;
            $node->setChildren($children);
        }


        if($node->getNodeOperation()->numberOfChildren() == 2){

            if($r2 == 0){

                $children[1] = new Node($node);
                $children[1]->setNodeOperation($this->randNonTerminated($operations));
                $children[1]->setDepth($currentDepth);
                $this->createGrowMethodNode($children[1], $currentDepth, $maxDepth, $operations, $numberOfVariables);
            }else{

                $children[1] = new Node($node);
                $children[1]->setNodeOperation($this->randTerminated($numberOfVariables));
                $children[1]->setDepth($currentDepth);
            }
        }
    }

    /**
     * @param int $depth
     * @param ArrayCollection $operations
     * @param int $numberOfVariables
     * @return Solution
     */
    public function createGrowMethod(int $depth, ArrayCollection $operations, int $numberOfVariables){

        $solution = new Solution();
        $node = new Node(null);
        $node->setNodeOperation($this->randNonTerminated($operations));

        $node->setDepth(1);

        $solution->setHead($node);

        $this->createGrowMethodNode($node, 1, $depth, $operations, $numberOfVariables);

        return $solution;

    }

    public function createFullMethod(int $depth, ArrayCollection $operations, int $numberOfVariables){

        $solution = new Solution();
        $node = new Node();
        $node->setNodeOperation($this->randNonTerminated($operations));

        $node->setDepth(1);

        $solution->setHead($node);

        $this->createFullMethodNode($node, 1, $depth, $operations, $numberOfVariables);

        return $solution;
    }

    /**
     * @param ArrayCollection $operations
     * @return Operation
     */
    public function randNonTerminated(ArrayCollection $operations){
        $operationId = mt_rand(0, $operations->count()-1);

        $operationName = $operations[$operationId];

        switch($operationName){
            case 'ADD':
                $class = new OP_ADD();
                break;
            case 'DIV':
                $class = new OP_DIV();
                break;
            case 'DIVp':
                $class = new OP_DIV_P();
                break;
            case 'MUL':
                $class = new OP_MUL();
                break;
            case 'SUB':
                $class = new OP_SUB();
                break;
            case 'POW':
                $class = new OP_POW();
                break;
            case 'POWp':
                $class = new OP_POW_P();
                break;
            case 'SIN':
                $class = new OP_SIN();
                break;
            case 'COS':
                $class = new OP_COS();
                break;
            case 'EXP':
                $class = new OP_EXP();
                break;
            case 'LOG':
                $class = new OP_LOG();
                break;
            case 'LOGp':
                $class = new OP_LOG_P();
                break;
            default: $class = new OP_ADD();
                break;
        }

        return $class;
    }

    public function randTerminated(int $numberOfVariables){

        $operationID = mt_rand(0,3);

        if($operationID == 1){

            $operation = new OP_T_V();

            if($numberOfVariables != 1){
                $a = 0;
                $a++;
            }

            $operation->init($numberOfVariables);

            return $operation;
        }else{

            $operation = new OP_T_D();
            $operation->init(5);
            return $operation;
        }
    }

    /**
     * @param Solution $p1o
     * @param Solution $p2o
     * @param int $maxDepth
     * @param Solution $np1
     * @param Solution $np2
     */
    public function crossOver(Solution $p1o, Solution $p2o, int $maxDepth, Solution &$np1 = null, Solution &$np2 = null){
        /** @var Solution $p1 */
        $p1 = null;
        /** @var Solution $p2 */
        $p2 = null;
        /** @var bool $finish */
        $finish = false;

        do{

            $p1 = $p1o->getCloned();
            $p2 = $p2o->getCloned();

            $numberOfNodesP1 = $p1->getHead()->countNodes(0);
            $numberOfNodesP2 = $p2->getHead()->countNodes(0);

            $nodeID1 = mt_rand(0, $numberOfNodesP1 - 2) + 1;
            $nodeID2 = mt_rand(0, $numberOfNodesP2 - 2) + 1;


            $node1 = $p1->getHead()->selectNode(0, $nodeID1);

            $node2 = $p2->getHead()->selectNode(0, $nodeID2);

            $parent1 = $node1->getParent();
            $parent2 = $node2->getParent();

            if($parent1->getChildren()[0] === $node1){

                $parent1->getChildren()[0] = $node2;
            }else{

                $parent1->getChildren()[1] = $node2;
            }

            if($parent2->getChildren()[0] === $node2){

                $parent2->getChildren()[1] = $node1;
            }else{

                $parent2->getChildren()[1] = $node1;
            }

            $d1 = $p1->getHead()->getPathLength(0);
            $d2 = $p2->getHead()->getPathLength(0);

            $s1 = $p1->getHead()->getDescription();
            $s2 = $p2->getHead()->getDescription();


            $s3 = $p1o->getHead()->getDescription();
            $s4 = $p2o->getHead()->getDescription();

            if($d1 <= $maxDepth && $d2 <= $maxDepth){
                $finish = true;
            }

        }while(!$finish);

        $np1 = $p1;
        $np2 = $p2;
    }

    public function mutation(Solution $po, int $maxDepth, ArrayCollection $operations, int $numberOfVariables, Solution &$np1 = null){

        /** @var Solution $p */
        $p = null;
        /** @var bool $finish */
        $finish = false;

        do{

            $p = $po->getCloned();

            $p->getHead()->getPathLength(0);
            $numberOfNodes = $p->getHead()->countNodes(0);
            $nodeID = mt_rand(0, $numberOfNodes-2) + 1;

            $node = $p->getHead()->selectNode(0, $nodeID);

            $parent = $node->getParent();

            $r = mt_rand(0, 3);

            if($parent->getChildren()[0] === $node){

                $parent->getChildren()[0] = new Node($parent);
                $parent->getChildren()[0]->setDepth($parent->getDepth()+1);
                $parent->getChildren()[0]->setNodeOperation($this->randNonTerminated($operations));

                if($r == 0){

                    $this->createFullMethodNode($parent->getChildren()[0], $parent->getChildren()[0]->getDepth(), $maxDepth, $operations, $numberOfVariables);
                }else{

                    $this->createGrowMethodNode($parent->getChildren()[0], $parent->getChildren()[0]->getDepth(), $maxDepth, $operations, $numberOfVariables);
                }

            }else{

                $parent->getChildren()[1] = new Node($parent);
                $parent->getChildren()[1]->setDepth($parent->getDepth()+1);
                $parent->getChildren()[1]->setNodeOperation($this->randNonTerminated($operations));

                if($r == 0){

                    $this->createFullMethodNode($parent->getChildren()[1], $parent->getChildren()[1]->getDepth(), $maxDepth, $operations, $numberOfVariables);
                }else{

                    $this->createGrowMethodNode($parent->getChildren()[1], $parent->getChildren()[1]->getDepth(), $maxDepth, $operations, $numberOfVariables);
                }
            }

            $d = $p->getHead()->getPathLength(0);

            if($d <= $maxDepth){
                $finish = true;
            }

        }while(!$finish);

        $np1 = $p;
    }






    /**
     * @param ArrayCollection $array
     * @param int $row
     * @return ArrayCollection
     */
    public function removeRowFromArray(ArrayCollection $array, int $row){
        $newArray = new ArrayCollection();
        for($i = 0; $i< $array->count(); $i++){
            if($i != $row)
                $newArray->add($array->get($i));
        }
        return $newArray;
    }

}
