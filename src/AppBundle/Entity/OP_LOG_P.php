<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * OP_LOG_P
 */
class OP_LOG_P extends Operation
{

    /**
     * @param null $values
     * @param null $data
     * @param null $error
     * @return float|int
     */
    public function result($values = null, $data = null, &$error = null){

        $d = log10($data[0]);

        $error = false;

        if(is_nan($d)){
            return 1;
        }else{
            return $d;
        }
    }

    /**
     * @return bool
     */
    public function isTerminated(){
        return false;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return 'LOGp';
    }

    /**
     * @return int
     */
    public function numberOfChildren(){
        return 1;
    }

    /**
     * @return OP_LOG_P
     */
    public function getCloneOperation(){
        return new OP_LOG_P();
    }

}
